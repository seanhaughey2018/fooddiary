﻿using FoodDiary.Data.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDiary.Data
{
    public interface IFoodDiaryRepository
    {
        IQueryable<Food> GetAllFoods();
        IQueryable<Food> GetAllFoodsWithMeasures();
        Food GetFood(int id);
        Measure GetMeasure(int id);
        IQueryable<Measure> GetMeasuresForFood(int foodId);
        IQueryable<Diary> GetDiaries(string userName);
        Diary GetDiary(string userName, DateTime date );
        IQueryable<DiaryEntry> GetDiaryEntries(String userName, DateTime diaryDay);
        DiaryEntry GetDiaryEntry(string userName, DateTime diaryDay, int id);
       bool SaveAll();
        bool DeleteDiaryEntry(int id);
    }
}
