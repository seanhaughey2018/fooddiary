﻿using FoodDiary.Data.Entites;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDiary.Data
{
    public class FoodDiaryMapping
    { 
    public static void Configure(DbModelBuilder modelBuilder)
    {
        MapFood(modelBuilder);
        MapMeasure(modelBuilder);
        MapDiaryEntry(modelBuilder);
        MapDiary(modelBuilder);
       
    }

    static void MapFood(DbModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Food>().ToTable("Food", "Nutrition");
    }

    static void MapMeasure(DbModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Measure>().ToTable("Measure", "Nutrition");
    }

    static void MapDiaryEntry(DbModelBuilder modelBuilder)
    {
        modelBuilder.Entity<DiaryEntry>().ToTable("DiaryEntry", "FoodDiaries");
    }

    static void MapDiary(DbModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Diary>().ToTable("Diary", "FoodDiaries");
    }

}
}
