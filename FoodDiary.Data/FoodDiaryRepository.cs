﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodDiary.Data.Entites;

namespace FoodDiary.Data
{
    public class FoodDiaryRepository : IFoodDiaryRepository
    {
        private FoodDiaryContext _ctx;

        public FoodDiaryRepository(FoodDiaryContext ctx)
        {
            _ctx = ctx;
        }

        public bool DeleteDiaryEntry(int id)
        {
            try
            {
                var entity = _ctx.DiaryEntries.Where(f => f.Id == id).FirstOrDefault();
                if (entity != null)
                {
                    _ctx.DiaryEntries.Remove(entity);
                    return true;
                }
            }
            catch
            {
                Console.WriteLine("put in nlog ");
            }

            return false;
        }

        public IQueryable<Food> GetAllFoods()
        {
            return _ctx.Foods;
        }

        public IQueryable<Food> GetAllFoodsWithMeasures()
        {
            return _ctx.Foods.Include("Measures");
        }

        public IQueryable<Diary> GetDiaries(string userName)
        {
            return _ctx.Diaries.Include("Entries.FoodItem")
                .Include("Entries.Measure")
                .OrderByDescending(d => d.CurrentDate)
                .Where(d => d.UserName == userName);
        }

        
        public Diary GetDiary(string userName, DateTime day)
        {
            return GetDiaries(userName).Where(d => d.CurrentDate == day.Date).FirstOrDefault();
        }

        public IQueryable<DiaryEntry> GetDiaryEntries(string userName, DateTime diaryDay)
        {
            return _ctx.DiaryEntries.Include("FoodItem")
                              .Include("Measure")
                              .Include("Diary")
                              .Where(f => f.Diary.UserName == userName &&
                                          f.Diary.CurrentDate == diaryDay);
        }

        public DiaryEntry GetDiaryEntry(string userName, DateTime diaryDay, int id)
        {
            return _ctx.DiaryEntries.Include("FoodItem")
                             .Include("Measure")
                             .Include("Diary")
                             .Where(f => f.Diary.UserName == userName &&
                                         f.Diary.CurrentDate == diaryDay &&
                                         f.Id == id)
                             .FirstOrDefault();
        }

        public Food GetFood(int id)
        {
            return _ctx.Foods.Include("Measures").Where(c => c.Id == id).FirstOrDefault();
        }

        public Measure GetMeasure(int id)
        {
            return _ctx.Measures.Include("Food")
                .Where(m => m.Id == id)
                .FirstOrDefault();
        }

        public IQueryable<Measure> GetMeasuresForFood(int foodId)
        {
            return _ctx.Measures.Include("Food")
                          .Where(m => m.Food.Id == foodId);
        }

        public bool SaveAll()
        {
            return _ctx.SaveChanges() > 0;
        }
    }
}
