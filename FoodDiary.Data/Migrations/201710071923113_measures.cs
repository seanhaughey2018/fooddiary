namespace FoodDiary.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class measures : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Measures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Calories = c.Double(nullable: false),
                        Fats = c.Double(nullable: false),
                        Protein = c.Double(nullable: false),
                        Carbohydrates = c.Double(nullable: false),
                        Food_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Foods", t => t.Food_Id)
                .Index(t => t.Food_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Measures", "Food_Id", "dbo.Foods");
            DropIndex("dbo.Measures", new[] { "Food_Id" });
            DropTable("dbo.Measures");
        }
    }
}
