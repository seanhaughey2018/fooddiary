namespace FoodDiary.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class diarydiaryentrs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Diaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CurrentDate = c.DateTime(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DiaryEntries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Double(nullable: false),
                        Diary_Id = c.Int(),
                        FoodItem_Id = c.Int(),
                        Measure_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Diaries", t => t.Diary_Id)
                .ForeignKey("dbo.Foods", t => t.FoodItem_Id)
                .ForeignKey("dbo.Measures", t => t.Measure_Id)
                .Index(t => t.Diary_Id)
                .Index(t => t.FoodItem_Id)
                .Index(t => t.Measure_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DiaryEntries", "Measure_Id", "dbo.Measures");
            DropForeignKey("dbo.DiaryEntries", "FoodItem_Id", "dbo.Foods");
            DropForeignKey("dbo.DiaryEntries", "Diary_Id", "dbo.Diaries");
            DropIndex("dbo.DiaryEntries", new[] { "Measure_Id" });
            DropIndex("dbo.DiaryEntries", new[] { "FoodItem_Id" });
            DropIndex("dbo.DiaryEntries", new[] { "Diary_Id" });
            DropTable("dbo.DiaryEntries");
            DropTable("dbo.Diaries");
        }
    }
}
