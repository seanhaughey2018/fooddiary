namespace FoodDiary.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Diaries", newName: "Diary");
            RenameTable(name: "dbo.DiaryEntries", newName: "DiaryEntry");
            RenameTable(name: "dbo.Foods", newName: "Food");
            RenameTable(name: "dbo.Measures", newName: "Measure");
            MoveTable(name: "dbo.Diary", newSchema: "FoodDiaries");
            MoveTable(name: "dbo.DiaryEntry", newSchema: "FoodDiaries");
            MoveTable(name: "dbo.Food", newSchema: "Nutrition");
            MoveTable(name: "dbo.Measure", newSchema: "Nutrition");
        }
        
        public override void Down()
        {
            MoveTable(name: "Nutrition.Measure", newSchema: "dbo");
            MoveTable(name: "Nutrition.Food", newSchema: "dbo");
            MoveTable(name: "FoodDiaries.DiaryEntry", newSchema: "dbo");
            MoveTable(name: "FoodDiaries.Diary", newSchema: "dbo");
            RenameTable(name: "dbo.Measure", newName: "Measures");
            RenameTable(name: "dbo.Food", newName: "Foods");
            RenameTable(name: "dbo.DiaryEntry", newName: "DiaryEntries");
            RenameTable(name: "dbo.Diary", newName: "Diaries");
        }
    }
}
