﻿namespace FoodDiary.Data
{
    public class Measure
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Calories { get; set; }
        public double Fats { get; set; }
        public double Protein { get; set; }
        public double Carbohydrates { get; set; }
        public virtual Food Food { get; set; }
    }
}