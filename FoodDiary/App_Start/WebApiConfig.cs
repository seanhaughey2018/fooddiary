﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Formatting;
using FoodDiary.Filter;
using WebApiContrib.Formatting.Jsonp;

namespace FoodDiary
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
               name: "Food",
               routeTemplate: "api/nutrition/foods/{foodid}",
               defaults: new { controller = "Foods", foodid = RouteParameter.Optional }
           );

            config.Routes.MapHttpRoute(
               name: "Measures",
               routeTemplate: "api/nutrition/foods/{foodid}/measures/{id}",
               defaults: new { controller = "measures", id = RouteParameter.Optional }
           );

            config.Routes.MapHttpRoute(
               name: "Diaries",
               routeTemplate: "api/user/diaries/{diaryid}",
               defaults: new { controller = "diaries", diaryid = RouteParameter.Optional }
           );

            config.Routes.MapHttpRoute(
                name: "DiaryEntries",
                routeTemplate: "api/user/diaries/{diaryid}/entries/{id}",
                defaults: new { controller = "diaryentries", id = RouteParameter.Optional }
             );

            config.Routes.MapHttpRoute(
                name: "DiarySummary",
                routeTemplate: "api/user/diaries/{diaryid}/summary",
                defaults: new { controller = "diarysummary" }
            );


            /*config.Routes.MapHttpRoute(
            name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );*/

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().FirstOrDefault();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //add support JSONP
            var formatter = new JsonpMediaTypeFormatter(jsonFormatter);
            config.Formatters.Insert(0, formatter);

#if !DEBUG
            //force https on entire api
            config.Filters.Add(new RequireHttpsAttritube());
#endif
        }
    }
}
