﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDiary.Services
{
    public interface IFoodDiaryIdentityService
    {
        string CurrentUser { get; }
    }
}
