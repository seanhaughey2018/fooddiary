﻿using FoodDiary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Routing;
using FoodDiary.Data.Entites;
using FoodDiary.Controllers;

namespace FoodDiary.Models
{
    public class ModelFactory
    {
        private UrlHelper _urlHelper;
        private IFoodDiaryRepository _repo;

        public ModelFactory(HttpRequestMessage request, IFoodDiaryRepository repo)
        {
            _urlHelper = new UrlHelper(request);
            _repo = repo;
        }
        public FoodModel Create (Food food)
        {
            return new FoodModel()
            {
                Url = _urlHelper.Link("Food",new {foodid = food.Id }),
                Description = food.Description,
                Measures = food.Measures.Select(m => Create(m))  
            };
        }

        public MeasureModel Create(Measure measure)
        {
            return new MeasureModel()
            {
                Url = _urlHelper.Link("Food", new { foodid = measure.Food.Id, id = measure.Id }),
                Description = measure.Description,
                Calories = measure.Calories
            };
        }

        public DiarySummaryModel CreateSummary(Diary diary)
        {
            return new DiarySummaryModel()
            {
                DiaryDate = diary.CurrentDate,
                TotalCalories = Math.Round(diary.Entries.Sum(e => e.Measure.Calories * e.Quantity))
            };
        }

        public DiaryEntryModel Create(DiaryEntry entry)
        {
            return new DiaryEntryModel()
            {
                Url = _urlHelper.Link("DiaryEntries",
                    new { diaryid = entry.Diary.CurrentDate.ToString("yyyy-MM-dd"), id = entry.Id }),
                Quantity = entry.Quantity,
                FoodDescription = entry.FoodItem.Description,
                MeasureDescription = entry.Measure.Description,
                MeasureUrl = _urlHelper.Link("Measures", new { foodid = entry.FoodItem.Id, id = entry.Measure.Id })
            };
        }

        public DiaryEntry Parse(DiaryEntryModel model)
        {
            try
            {
                var entry = new DiaryEntry();

                if (model.Quantity != default(double))
                {
                    entry.Quantity = model.Quantity;
                }

                if (!string.IsNullOrEmpty(model.MeasureUrl))
                {
                    var uri = new Uri(model.MeasureUrl);
                    var measureId = int.Parse(uri.Segments.Last());
                    var measure = _repo.GetMeasure(measureId);
                    entry.Measure = measure;
                    entry.FoodItem = measure.Food;
                }

                return entry;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public DiaryModel Create(Diary d)
        {

            return new DiaryModel()
            {
                Url = _urlHelper.Link("Diaries", new { diaryid = d.CurrentDate.ToString("dd-MM-yyyy")}), 
                CurrentDate = d.CurrentDate
            } ;
                
        }

        
    }
}