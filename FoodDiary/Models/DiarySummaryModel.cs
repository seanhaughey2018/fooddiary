﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodDiary.Models
{
    public class DiarySummaryModel
    {
        public DateTime DiaryDate { get; set; }
        public double TotalCalories { get; set; }
    }
}