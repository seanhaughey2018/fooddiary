﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FoodDiary.Data;
using FoodDiary.Services;
using FoodDiary.Models;

namespace FoodDiary.Controllers
{
    public class DiarySummaryController : BaseApiController
    {
        private IFoodDiaryIdentityService _identityService;

        public DiarySummaryController(IFoodDiaryRepository repo, IFoodDiaryIdentityService identityService) : base(repo)
        {
            _identityService = identityService;
        }

        public object Get(DateTime diaryId)
        {
            try
            {
                var diary = TheRepository.GetDiary(_identityService.CurrentUser, diaryId);

                if (diary == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return TheModelFactory.CreateSummary(diary);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
