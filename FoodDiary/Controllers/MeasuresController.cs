﻿using FoodDiary.Data;
using FoodDiary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FoodDiary.Controllers
{
    public class MeasuresController :BaseApiController
    {
        //private ModelFactory _modelFactory;
        //private IFoodDiaryRepository _repo;

        public MeasuresController(IFoodDiaryRepository repo):base(repo)
        {
            //_repo = repo;
            //_modelFactory = new ModelFactory();
        }


        //http://localhost:52671/api/nutrition/foods/1/measures
        public IEnumerable<MeasureModel>Get(int foodid)
        {
            var results = TheRepository.GetMeasuresForFood(foodid)
                .ToList()
                .Select(m => TheModelFactory.Create(m));

            return results;
        }


        //http://localhost:52671/api/nutrition/foods/1/measures/1
        public MeasureModel Get(int foodid, int id)
        {
            var results = TheRepository.GetMeasure(id);

            if(results.Food.Id == foodid)
            {
                return TheModelFactory.Create(results);
            }

            return null;
        }
    }
}
