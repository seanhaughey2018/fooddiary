﻿using FoodDiary.Data;
using FoodDiary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace FoodDiary.Controllers
{
    
    public class FoodsController : BaseApiController
    {
       // private ModelFactory _modelFactory;
        //private IFoodDiaryRepository _repo;

        public FoodsController(IFoodDiaryRepository repo) :base(repo)
        {
          //  _repo = repo;
            //_modelFactory = new ModelFactory(this.Request);
        }

        /*public IEnumerable<object>Get()
        {
            var result = _repo.GetAllFoods()
                .OrderBy(f => f.Description)
                .ToList()
                .Select(f => new
                {
                    Description = f.Description,
                    Measures = f.Measures.Select(m =>
                    new
                    {
                        Description = m.Description,
                        Calories = m.Calories
                    })
                });

            return result;
        }*/
        //http://localhost:52671/api/nutrition/foods
        const int PAGE_SIZE = 25;
        public IEnumerable<FoodModel> Get(bool includeMeasures = true, int page = 0)
        {

            IQueryable<Food> query;

            if(includeMeasures)
            {
                query = TheRepository.GetAllFoodsWithMeasures();
            }
            else
            {
                query = TheRepository.GetAllFoods();
            }

            var baseQuery = query.OrderBy(f => f.Description);

            var totalCount = baseQuery.Count();

            var totalPages = Math.Ceiling((double)totalCount / PAGE_SIZE);


            var result = TheRepository.GetAllFoodsWithMeasures()
                .OrderBy(f => f.Description)
                .Skip(PAGE_SIZE * page)
                .Take(PAGE_SIZE)
                .ToList()
                .Select(f => TheModelFactory.Create(f));


            return result;
        }

        //http://localhost:52671/api/nutrition/foods/1
        public FoodModel Get(int foodid)
        {
            return TheModelFactory.Create(TheRepository.GetFood(foodid));
        }
    }
}


/*public object Get(bool includeMeasures = true, int page = 0)
        {
            IQueryable<Food> query;

            if (includeMeasures)
            {
                query = TheRepository.GetAllFoodsWithMeasures();
            }
            else
            {
                query = TheRepository.GetAllFoods();
            }

            var baseQuery = query.OrderBy(f => f.Description);

            var totalCount = baseQuery.Count();
            var totalPages = Math.Ceiling((double)totalCount / PAGE_SIZE);

            var helper = new UrlHelper(Request);
            var prevUrl = page > 0 ? helper.Link("Food", new { page = page - 1 }) : "";
            var nextUrl = page < totalPages - 1 ? helper.Link("Food", new { page = page + 1 }) : "";

            var results = baseQuery.Skip(PAGE_SIZE * page)
                .Take(PAGE_SIZE)
                .ToList()
                .Select(f => TheModelFactory.Create(f));

            return new
            {
                TotalCount = totalCount,
                TotalPage = totalPages,
                PrevPageUrl = prevUrl,
                NextPageUrl = nextUrl,
                Results = results
            };
        }
*/