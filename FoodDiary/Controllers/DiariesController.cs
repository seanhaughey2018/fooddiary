﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FoodDiary.Data;
using FoodDiary.Services;
using FoodDiary.Models;

namespace FoodDiary.Controllers
{
    public class DiariesController : BaseApiController
    {
        private IFoodDiaryIdentityService _identityService;

        public DiariesController(IFoodDiaryRepository repo, IFoodDiaryIdentityService identityService) : base(repo)
        {
            _identityService = identityService;
        }

        public IEnumerable<DiaryModel> Get()
        {
            var username = _identityService.CurrentUser;
            var results = TheRepository.GetDiaries(username)
                .OrderByDescending(d => d.CurrentDate)
                .ToList()
                .Select(d => TheModelFactory.Create(d));

            return results;

        }

        public HttpResponseMessage Get(DateTime diaryId)
        {
            var username = _identityService.CurrentUser;
            var result = TheRepository.GetDiary(username, diaryId);

            if(result == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, TheModelFactory.Create(result));

        }

        public object Post(DateTime diaryId, [FromBody]DiaryEntryModel model)
        {
            return null;
        }
    }
}
