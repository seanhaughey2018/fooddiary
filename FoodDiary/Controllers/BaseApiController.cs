﻿using FoodDiary.Data;
using FoodDiary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FoodDiary.Controllers
{
    public abstract class BaseApiController:ApiController
    {
        private ModelFactory _modelFactory;
        IFoodDiaryRepository _repo;

        public BaseApiController(IFoodDiaryRepository repo)
        {
            _repo = repo;
        }

        protected IFoodDiaryRepository TheRepository
        {
            get
            {
                return _repo;
            }
        }


        //deferr the action
        protected ModelFactory TheModelFactory
        {
            get
            {
                if(_modelFactory ==null)
                {
                    _modelFactory = new ModelFactory(this.Request, TheRepository);
                }

                return _modelFactory;
            }
        }
    }
}